<?php

namespace Drupal\commerce_satispay_business\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsRefundsInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\PaymentGatewayInterface;

/**
 * Provides the interface for the Satispay payment gateway.
 */
interface SatispayOnlineInterface extends PaymentGatewayInterface, SupportsRefundsInterface {

  /**
   * Payment API request used in the offsite form.
   *
   * Builds the data for the request and make the request.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment.
   * @param array $extra
   *   Extra data needed for this request.
   *
   * @return array
   *   Satispay response data.
   *
   * @see https://
   */
  public function createPayment(PaymentInterface $payment, array $extra);

  /**
   * Process Satispay payment.
   *
   * @param \Drupal\commerce_satispay_business\Plugin\Commerce\PaymentGateway\string $uuid
   *   Payment ID.
   *
   * @return mixed
   *   Payment processed.
   */
  public function processPayment(string $uuid);

  /**
   * Retrieve Url used for async comunications, it corresponds to the onNotify url.
   */
  public function getCallbackUrl();

}
