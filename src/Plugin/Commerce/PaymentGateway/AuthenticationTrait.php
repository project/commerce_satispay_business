<?php

namespace Drupal\commerce_satispay_business\Plugin\Commerce\PaymentGateway;

use SatispayGBusiness\Api;
use SatispayGBusiness\ApiAuthentication;

/**
 * Satispay authentication.
 */
trait AuthenticationTrait {

  /**
   * Satispay key name.
   *
   * @var string
   */
  protected $keyName = 'satispay';

  /**
   * Get.
   */
  public function getAuthentication() {
    try {
      /** @var \SatispayGBusiness\ApiAuthentication */
      return json_decode($this->key->getKey($this->keyName)->getKeyValue());
    }
    catch (\Exception $ex) {
      return FALSE;
    }
  }

  /**
   * Save.
   */
  public function setAuthentication(ApiAuthentication $authentication) {
    $satispay_key = $this->key->getKey($this->keyName);
    $satispay_key->setKeyValue(json_encode($authentication, JSON_PRETTY_PRINT));
    $satispay_key->save();
  }

  /**
   * Validate authentication.
   *
   * @return bool
   *   TRUE if it's valid, otherwise FALSE.
   */
  public function isValidAuthentication() {
    /** @var \SatispayGBusiness\ApiAuthentication $auth */
    $auth = $this->getAuthentication();
    return isset($auth->keyId) && isset($auth->publicKey) && isset($auth->privateKey);
  }

  /**
   * Validate if .
   */
  public function validateAuthentication() {
    $config = $this->getConfiguration();
    try {
      // If authentication has been already generated we don't do it again
      // unless we want to refresh the token.
      if ($config['refresh'] || !$this->isValidAuthentication()) {
        Api::setSandbox($config['mode'] == 'test');
        $response = Api::authenticateWithToken($config['token']);
        $this->logger->notice('Satispay authentication succeded using token @token.', ['@token' => $config['token']]);
        $this->setAuthentication($response);
      }
      return TRUE;
    }
    catch (\Exception $ex) {
      $this->logger->error('Satispay authentication error using token @token', ['@token' => $config['token']]);
      return FALSE;
    }
  }

  /**
   * Auth.
   */
  public function authenticate() {
    /** @var \SatispayGBusiness\ApiAuthentication $authentication */
    $authentication = $this->getAuthentication();
    $config = $this->getConfiguration();

    Api::setPlatformHeader('Drupal');
    Api::setPlatformVersionHeader(\Drupal::VERSION);
    Api::setPrivateKey($authentication->privateKey);
    Api::setPublicKey($authentication->publicKey);
    Api::setKeyId($authentication->keyId);
    Api::setSandbox($config['mode'] == 'test');

    if ($config['logging']) {
      $this->logger->notice('Satispay Authentication: mode @mode', ['@mode' => $config['mode']]);
    }
  }

}
