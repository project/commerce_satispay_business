<?php

namespace Drupal\commerce_satispay_business\Plugin\Commerce\PaymentGateway;

use Drupal\Core\Form\FormStateInterface;

/**
 * Satispay configuration form.
 */
trait ConfigurationFormTrait {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'token' => '',
      'logging' => FALSE,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    if ($this->isValidAuthentication()) {
      $form['auth'] = [
        '#type' => 'details',
        '#title' => $this->t('Refresh Authentication Details'),
        '#description' => $this->t('Hey, seems like the Satispay API configuration is setted up already. If you want to refresh the authentication details (public_key, private_key and key_id) please click on the following checkbox, but be extremely careful when editing the token, it may result in broken site functionality. Check the <a href="@docs">Satispay documentation</a> for more details about the authentication process.', ['@docs' => 'https://developers.satispay.com/docs/getting-started']),
        '#open' => TRUE,
      ];

      $form['auth']['refresh'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Refresh authentication details'),
      ];

      $form['auth']['info'] = [
        '#type' => 'item',
        '#title' => $this->t('Please note'),
        '#markup' => $this->t('Satisapy tokens are <b>disposable</b>, so they can be used only once to generate proper authentication details. If you want to proceed anyway, please select the Refresh option to refresh the authentication details using a new token.'),
        '#states' => [
          'visible' => [
            ':input[name="configuration[satispay_online][token]"]' => ['disabled' => TRUE],
          ],
        ],
      ];
    }

    $form['token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Activation code token'),
      '#description' => $this->t('Check out the <a href="@docs">Satispay documentation</a> on how to generate an Activation code from your online shop.', ['@docs' => 'https://developers.satispay.com/docs/business-account']),
      '#default_value' => $this->configuration['token'],
      '#required' => TRUE,
      '#maxlength' => 255,
      '#size' => 150,
      // '#attributes' => ['name' => 'token'],
      '#states' => [
        'disabled' => [
          // If we are trying to edit the token we must be sure to refresh the authentication to make it sure properly
          // this forces the user to double-check the Refresh Authentication Details.
          ':input[name="configuration[satispay_online][token]"]' => ['filled' => TRUE],
          'and' => 'and',
          ':input[name="configuration[satispay_online][auth][refresh]"]' => ['checked' => FALSE],
        ],
      ],
    ];

    $form['logging'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable debug log for Satispay API requests'),
      '#description' => $this->t('Debug logs will be available <a href="@logs">here</a>.', ['@logs' => '/admin/reports/dblog?type%5B%5D=commerce_satispay_business']),
      '#default_value' => $this->configuration['logging'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);

    if (!$form_state->getErrors() && $form_state->isSubmitted()) {
      $values = $form_state->getValue($form['#parents']);

      $this->configuration['token'] = $values['token'];
      $this->configuration['logging'] = $values['logging'];
      $this->configuration['mode'] = $values['mode'];
      // We need this only for checking if we have to re-authenticate using the token
      // even if there are already some authorization settings in place.
      $this->configuration['refresh'] = isset($values['auth']) ? $values['auth']['refresh'] : FALSE;

      if (!$this->validateAuthentication()) {
        $form_state->setError($form['token'], $this->t('There is an error with the Authentication Token provided, please try with a different one.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['token'] = $values['token'];
      $this->configuration['logging'] = $values['logging'];
      $this->configuration['mode'] = $values['mode'];
    }
  }

}
