<?php

namespace Drupal\commerce_satispay_business\Plugin\Commerce\PaymentGateway;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\commerce_price\Price;
use Drupal\key\KeyRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;
use SatispayGBusiness\Payment;

/**
 * Provides the Satispay payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "satispay_online",
 *   label =  @Translation("Satispay Business"),
 *   display_label = @Translation("Satispay"),
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_satispay_business\PluginForm\SatispayOnlineForm",
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "amex", "dinersclub", "discover", "jcb", "maestro", "mastercard", "visa",
 *   },
 * )
 */
class SatispayOnline extends OffsitePaymentGatewayBase implements SatispayOnlineInterface {

  use AuthenticationTrait;
  use ConfigurationFormTrait;

  /**
   * Keeps track of status names mapping.
   *
   * @var array
   */
  private $statusMapping = [
    'PENDING' => 'authorization',
    'ACCEPTED' => 'completed',
    'CANCELED' => 'failed',
  ];

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Constructs a new PaymentGatewayBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce_payment\PaymentTypeManager $payment_type_manager
   *   The payment type manager.
   * @param \Drupal\commerce_payment\PaymentMethodTypeManager $payment_method_type_manager
   *   The payment method type manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_channel_factory
   *   The logger channel factory.
   * @param \Drupal\key\KeyRepository $key
   *   Key repository.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, PaymentTypeManager $payment_type_manager, PaymentMethodTypeManager $payment_method_type_manager, TimeInterface $time, LoggerChannelFactoryInterface $logger_channel_factory, KeyRepository $key) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time);
    $this->logger = $logger_channel_factory->get('commerce_satispay_business');
    $this->key = $key;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('logger.factory'),
      $container->get('key.repository')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function onCancel(OrderInterface $order, Request $request) {
    parent::onCancel($order, $request);
    // TODO: Implement onCancel() method.
  }

  /**
   * {@inheritdoc}
   */
  public function onNotify(Request $request) {
    parent::onNotify($request);
    // Getting the payment id from the query URL
    // The query option {uuid} will be replaced with the Payment ID by Satispay.

    // @see https://developers.satispay.com/reference#create-a-payment
    $satispay_id = $request->get('uuid');
    $this->processPayment($satispay_id);
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    parent::onReturn($order, $request);
    // Getting the payment id from the order data.
    $satispay_id = $order->getData('satispay_id');
    $this->processPayment($satispay_id);
  }

  /**
   * {@inheritdoc}
   */
  public function createPayment(PaymentInterface $payment, array $extra) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $payment->getOrder();
    /** @var \Drupal\commerce_price\Price $price */
    $price = $this->convertPrice($order->getTotalPrice());

    try {
      // Set necessary auth for requests.
      $this->authenticate();
      // Generate Payment.
      $satispay_payment = Payment::create([
        'flow' => 'MATCH_CODE',
        'description' => $this->t('Order #@order_number', ['@order_number' => $order->id()]),
        'amount_unit' => $price->getNumber(),
        'currency' => $price->getCurrencyCode(),
        'redirect_url' => $extra['return_url'],
        'callback_url' => $extra['callback_url'],
        'external_code' => $order->id(),
        'metadata' => [
          'order' => $order->id()
        ],
      ]);

      if ($this->getConfiguration()['logging']) {
        $this->logger->notice('Satispay Create Payment: @payment', ['@payment' => json_encode($satispay_payment)]);
      }

      return $satispay_payment;
    }
    catch (\Exception $ex) {
      // Log error wherever the payment failed.
      $this->logger->error('Payment error on order: @order', ['@order' => json_encode($order)]);
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function processPayment(string $satispay_id) {
    // // Exit now if the ID was empty.
    if (empty($satispay_id)) {
      $this->logger->error('Callback URL accessed with no Satispay UUID submitted.');
      throw new PaymentGatewayException('Callback URL accessed with no Satispay UUID submitted.');
    }

    try {
      $this->authenticate();
      $satispay_payment = Payment::get($satispay_id);
      // // Ensure we can load the existing corresponding transaction.
      $payment = $this->entityTypeManager->getStorage('commerce_payment')->loadByRemoteId($satispay_payment->id);

      // // If not, bail now because authorization transactions should be created
      // // by the API request itself.
      if (!$payment) {
        $this->logger->warning('Payment for order @order_number ignored: authorization transaction already created.', ['@order_number' => $order->id]);
        return FALSE;
      }

      if ($this->getConfiguration()['logging']) {
        $this->logger->notice('Satispay Process Payment: @payment', ['@payment' => json_encode($satispay_payment)]);
      }

      // Update the payment state.
      if (isset($this->status_mapping[$satispay_payment->status])) {
        $status = $this->status_mapping[$satispay_payment->status];
        $payment->setState($status);
      }

      $payment->setRemoteState($satispay_payment->status);
      $payment->save();
      return $payment;

    }
    catch (\Exception $ex) {
      // Log error wherever the payment failed.
      $this->logger->error('Payment error: @satispay_id', ['@satispay_id' => $satispay_id]);
      throw new PaymentGatewayException("Failed");
    }

  }

  /**
   * {@inheritdoc}
   */
  public function refundPayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['completed', 'partially_refunded']);
    // If not specified, refund the entire amount.
    $amount = $amount ?: $payment->getAmount();
    $price = $this->convertPrice($amount);
    $remote_id = $payment->getRemoteId();
    $this->assertRefundAmount($payment, $amount);

    // Perform the refund request here, throw an exception if it fails.
    try {
      $this->authenticate();
      Payment::create([
        'flow' => 'REFUND',
        'amount_unit' => $price->getNumber(),
        'currency' => $price->getCurrencyCode(),
        'parent_payment_uid' => $remote_id,
      ]);
    }
    catch (\Exception $e) {
      $this->logger->error('Error while refunding payment @payment', ['@payment' => $remote_id]);
      throw new PaymentGatewayException('Error while refunding.');
    }

    // Update payment state and refunded amount.
    $old_refunded_amount = $payment->getRefundedAmount();
    $new_refunded_amount = $old_refunded_amount->add($amount);
    $payment->state = $new_refunded_amount->lessThan($payment->getAmount())
      ? 'partially_refunded' : 'refunded';
    $payment->setRefundedAmount($new_refunded_amount);
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function getCallbackUrl() {
    $notify_url = $this->getNotifyUrl();
    $notify_url->setOption('query', ['uuid' => '{uuid}']);

    $callback_url = $notify_url->toString();
    $callback_url = str_replace('uuid=%7Buuid%7D', 'uuid={uuid}', $callback_url);
    return $callback_url;
  }

  /**
   * Convert price to Euro.
   *
   * @param \Drupal\commerce_price\Price $price
   *   Price amount.
   *
   * @return \Drupal\commerce_price\Price
   *   Price in Euro.
   */
  public function convertPrice(Price $price) {
    // Satispay only supports "EUR" currency
    // and accepts the amount unit in cents.
    return $price->convert('EUR', 100);
  }

}
